
let hotel = {
  Barcelona: {
    location: 'Barcelona',
    price: 1500,
    rooms: [1, 2, 3, 4]
  },
  Madrid: {
    location: 'Madrid',
    price: 1800,
    rooms: [1, 3, 4]
  },
  BuenosAires: {
    location: 'Buenos Aires',
    price: 1500,
    rooms: [1, 2, 5, 6, 10]
  },
  Salta: {
    location: 'Salta',
    price: 1300,
    rooms: [1, 2, 5, 6, 10]
  }
}


class Hotel {
  constructor(location, rooms, price) {
    this.location = location
    this.rooms = rooms
    this.price = price  
  }

  getRooms() {
    return parseInt(prompt(`Seleccione su habitacion:`))
  }
  
  selectRoomBarcelona(arr, numRoom) {
    let i = arr.indexOf(numRoom)
    if(i !== -1){
      arr.splice(i, 1)
    }
    if(this.location = 'Madrid' && this.rooms.length > 0){
      document.getElementById('roomsAvailableBarcelona').innerHTML = `Habitaciones disponibles: ${this.rooms}`
    } else {
      document.getElementById('roomsAvailableBarcelona').innerHTML = 'No hay habitaciones disponibles'
      document.getElementById('botonReservaBarcelona').disabled = true

     }
  }

  selectRoomMadrid(arr, room) {
    let i = arr.indexOf(numRoom)
    if(i !== -1){
      arr.splice(i, 1)
    }
    if(this.location = 'Barcelona' && this.rooms.length > 0){
      document.getElementById('roomsAvailableMadrid').innerHTML = `Habitaciones disponibles: ${this.rooms}`
    } else {
      document.getElementById('roomsAvailableMadrid').innerHTML = 'No hay habitaciones disponibles'
      document.getElementById('botonReservaMadrid').disabled = true
    }
  }

  selectRoomBuenosAires(arr, room) {
    let i = arr.indexOf(numRoom)
    if(i !== -1){
      arr.splice(i, 1)
    }
    if(this.location = 'Buenos Aires' && this.rooms.length > 0){
      document.getElementById('roomsAvailableBuenosAires').innerHTML = `Habitaciones disponibles: ${this.rooms}`
    } else {
      document.getElementById('roomsAvailableBuenosAires').innerHTML = 'No hay habitaciones disponibles'
      document.getElementById('botonReservaBuenosAires').disabled = true
    }
  }
  selectRoomSalta(arr, room) {
    let i = arr.indexOf(numRoom)
    if(i !== -1){
    arr.splice(i, 1)
    }
    if(this.location = 'Salta' && this.rooms.length > 0){
      document.getElementById('roomsAvailableSalta').innerHTML = `Habitaciones disponibles: ${this.rooms}`
    } else {
      document.getElementById('roomsAvailableSalta').innerHTML = 'No hay habitaciones disponibles'
      document.getElementById('botonReservaSalta').disabled = true
    }
  }
}


//LLamadas al objeto
let teloBarcelona = new Hotel(hotel.Barcelona.location, hotel.Barcelona.rooms, hotel.Barcelona.price)
let teloMadrid = new Hotel(hotel.Madrid.location, hotel.Madrid.rooms, hotel.Madrid.price)
let teloBuenosAires = new Hotel(hotel.BuenosAires.location, hotel.BuenosAires.rooms, hotel.BuenosAires.price)
let teloSalta = new Hotel(hotel.Salta.location, hotel.Salta.rooms, hotel.Salta.price)
 


//botonReserva
document.getElementById('hotelBarcelona').innerHTML = `Hotel ${teloBarcelona.location}`
document.getElementById('roomsAvailableBarcelona').innerHTML = `Habitaciones disponibles: ${teloBarcelona.rooms}`
document.getElementById('hotelDescriptionBarcelona').innerHTML = `$${teloBarcelona.price} por noche`

// Escucha de eventos
document.getElementById("botonReservaBarcelona").addEventListener("click", () =>    
  teloBarcelona.selectRoomBarcelona(teloBarcelona.rooms, teloBarcelona.getRooms()))

//botonReservaMadrid
document.getElementById('hotelMadrid').innerHTML = `Hotel ${teloMadrid.location}`
document.getElementById('roomsAvailableMadrid').innerHTML = `Habitaciones disponibles: ${teloMadrid.rooms}`
document.getElementById('hotelDescriptionMadrid').innerHTML = `$${teloMadrid.price} por noche`

// Escucha de eventos
document.getElementById("botonReservaMadrid").addEventListener("click", () => 
  teloMadrid.selectRoomMadrid(teloMadrid.rooms, teloMadrid.getRooms()))

//botonReserva2
document.getElementById('roomsAvailableBuenosAires').innerHTML = `Habitaciones disponibles: ${teloBuenosAires.rooms}`
document.getElementById('hotelDescriptionBuenosAires').innerHTML = `$${teloBuenosAires.price} por noche`

// Escucha de eventos
document.getElementById('botonReservaBuenosAires').addEventListener('click', () => 
  teloBuenosAires.selectRoomBuenosAires(teloBuenosAires.rooms, teloBuenosAires.getRooms()))


//botonReserva3
document.getElementById('roomsAvailableSalta').innerHTML = `Habitaciones disponibles: ${teloSalta.rooms}`
document.getElementById('hotelDescriptionSalta').innerHTML = `$${teloSalta.price} por noche`

// Escucha de eventos
document.getElementById("botonReservaSalta").addEventListener("click", () =>    
  teloSalta.selectRoomSalta(teloSalta.rooms, teloSalta.getRooms()))
  
  